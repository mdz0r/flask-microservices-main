import { Selector } from 'testcafe';

const TEST_URL = process.env.TEST_URL;

fixture('/all-users').page(`${TEST_URL}/all-users`);

test(`should display the page correctly if a user is not logged in`, async (t) => {
  await t
    .navigateTo(`${TEST_URL}/all-users`)
    .expect(Selector('a').withText('My Profile').exists).notOk()
    .expect(Selector('a').withText('My Genetics').exists).notOk()
    .expect(Selector('a').withText('Log Out').exists).notOk()
    .expect(Selector('a').withText('Log In').exists).ok()
    .expect(Selector('.alert').exists).notOk()
});
